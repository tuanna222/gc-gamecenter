package com.tealeaf.plugin.plugins;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import com.tealeaf.EventQueue;
import com.tealeaf.TeaLeaf;
import com.tealeaf.logger;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import java.util.HashMap;

import com.tealeaf.plugin.IPlugin;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.app.Dialog;
import android.widget.TextView;
import android.widget.Button;
import android.widget.TableLayout.LayoutParams;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;
import android.text.method.ScrollingMovementMethod;
import android.view.View;


import com.tealeaf.EventQueue;
import com.tealeaf.event.*;

import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;


public class GameCenterPlugin implements IPlugin {
    GameHelper mHelper;
    Activity mActivity;

	public GameCenterPlugin() {
	}

	public void onCreateApplication(Context applicationContext) {
	}

	public void onCreate(Activity activity, Bundle savedInstanceState) {
        mActivity = activity;
        PackageManager manager = activity.getPackageManager();
        try {
            Bundle meta = manager.getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData;
            mHelper = new GameHelper(activity, GameHelper.CLIENT_GAMES);
            GameHelper.GameHelperListener listener = new GameHelper.GameHelperListener() {
                @Override
                public void onSignInSucceeded() {
                    // handle sign-in succeess
                    logger.log("{gamecenter} onSignInSucceeded");
                }
                @Override
                public void onSignInFailed() {
                    // handle sign-in failure (e.g. show Sign In button)
                    logger.log("{gamecenter} onSignInFailed");
                }

            };
            mHelper.setup(listener);
        } catch (Exception e) {
            logger.log("{gamecenter} EXCEPTION",e.getMessage());
            e.printStackTrace();
        }
	}

    public void showLeaderBoard(final String json) {
        if(!mHelper.isSignedIn()){
            // this.showRequireSignInDialog();
            return;
        }
	    mActivity.runOnUiThread(new Runnable() {
            public void run() {
                try{
                    final JSONObject opts = new JSONObject(json);
                    String leaderBoardId = opts.getString("leaderBoardId");
                    if(leaderBoardId.equals("")){
                        mActivity.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(mHelper.getApiClient()),5000);
                    }else{
                        mActivity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mHelper.getApiClient(), leaderBoardId),5000);
                    }
                } catch (Exception e) {
                    logger.log("{gamecenter} Exception while processing showLeaderBoard:", e.getMessage());
                }
            }
        });
    }

    public void showAchievement(final String json) {
        if(!mHelper.isSignedIn()){
            // this.showRequireSignInDialog();
            return;
        }
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                try{
                    mActivity.startActivityForResult(Games.Achievements.getAchievementsIntent(mHelper.getApiClient()),5000);
                } catch (Exception e) {
                    logger.log("{gamecenter} Exception while processing showAchievement:", e.getMessage());
                }
            }
        });
    }

    public void postScore(final String json) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                try{
                    final JSONObject opts = new JSONObject(json);
                    String leaderBoardId = opts.getString("leaderBoardId");
                    Integer score = new Integer(opts.getString("score"));
                    Games.Leaderboards.submitScore(mHelper.getApiClient(), leaderBoardId, score);
                } catch (Exception e) {
                    logger.log("{gamecenter} Exception while processing postScore:", e.getMessage());
                }
            }
        });
    }

    public void postAchievement(final String json) {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                try{
                    final JSONObject opts = new JSONObject(json);
                    String achievementId = opts.getString("achievementId");
                    Games.Achievements.unlock(mHelper.getApiClient(), achievementId);
                } catch (Exception e) {
                    logger.log("{gamecenter} Exception while processing postAchievement:", e.getMessage());
                }
            }
        });
    }

	public void onResume() {
	}

	public void onStart() {
        logger.log("{gamecenter} connect to game center....");
        mHelper.onStart(mActivity);
	}

	public void onPause() {
	}

	public void onStop() {
        mHelper.onStop();
	}

	public void onDestroy() {
	}

	public void onNewIntent(Intent intent) {
	}

	public void setInstallReferrer(String referrer) {
	}

	public void onActivityResult(Integer request, Integer result, Intent data) {
        mHelper.onActivityResult(request, result, data);
	}

	public boolean consumeOnBackPressed() {
		return true;
	}

	public void onBackPressed() {
	}

    public void showRequireSignInDialog() {
        mActivity.runOnUiThread(new Runnable() {
            public void run() {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
                // set title
                alertDialogBuilder.setTitle("Game Center");
                // set dialog message
                alertDialogBuilder
                    .setMessage("Player is not signed in. Please sign in to google play game center!")
                    .setCancelable(false)
                    .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity
                            mHelper.reconnectClient();
                        }
                      })
                    .setNegativeButton("Not now",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }
        });
    }

}

