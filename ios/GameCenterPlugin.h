#import "PluginManager.h"
#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>


@interface GameCenterPlugin : GCPlugin <GKGameCenterControllerDelegate, GKChallengeListener, GKLocalPlayerListener> {
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
}
@property (nonatomic, retain) TeaLeafViewController *tealeafViewController;
@property (nonatomic, retain) TeaLeafAppDelegate *teaLeafAppDelegate;
@property (assign, readonly) BOOL gameCenterAvailable;
@property (nonatomic, strong) NSArray* leaderboards;
@property (nonatomic, strong) NSMutableDictionary *achievementsDictionary;

+ (GameCenterPlugin*)defaultHelper;
- (void)authenticateLocalUserOnViewController:(UIViewController*)viewController
                            setCallbackObject:(id)obj
                            withPauseSelector:(SEL)selector;

- (void)reportScore:(int64_t)score forLeaderboardID:(NSString*)identifier;
- (void)showLeaderboardOnViewController:(UIViewController*)viewController leaderBoardId:(NSString*)leaderBoardId;

- (void)reportAchievementIdentifier: (NSString*) identifier percentComplete: (float) percent;
- (GKAchievement*)getAchievementForIdentifier: (NSString*) identifier;
- (void)resetAchievements;
- (void)completeMultipleAchievements:(NSArray*)achievements;
- (void)registerListener:(id<GKLocalPlayerListener>)listener;

@end
