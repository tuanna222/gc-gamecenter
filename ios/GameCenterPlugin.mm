#import "GameCenterPlugin.h"

@interface GameCenterPlugin ()

- (void)loadLeaderBoardInfo;

@end

@implementation GameCenterPlugin

@synthesize gameCenterAvailable;
#pragma mark Initialization

static GameCenterPlugin *_sharedHelper = nil;

// The plugin must call super dealloc.
- (void) dealloc {
	[super dealloc];
}

//// The plugin must call super init.
//- (id) init {
//	self = [super init];
//	if (!self) {
//		return nil;
//	}
//	return self;
//}

- (void) initializeWithManifest:(NSDictionary *)manifest appDelegate:(TeaLeafAppDelegate *)appDelegate {
	@try {
        self.teaLeafAppDelegate = appDelegate;
        self.tealeafViewController = appDelegate.tealeafViewController;
        NSLog(@"{gamecenter} initializeWithManifest");
        [[GameCenterPlugin defaultHelper] authenticateLocalUserOnViewController:self.tealeafViewController
                                        setCallbackObject:self withPauseSelector:@selector(authenticationRequired)];
        [[GameCenterPlugin defaultHelper] registerListener:self];
	}
	@catch (NSException *exception) {
		NSLog(@"{gamecenter} error init: %@",exception);
	}
}

- (void)authenticationRequired
{
    //if the game is open, it should be paused
//    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    if (!self.teaLeafAppDelegate.wasPaused)
    {
        [self.teaLeafAppDelegate postPauseEvent:TRUE];
    }
}

+ (GameCenterPlugin*)defaultHelper {
    // dispatch_once will ensure that the method is only called once (thread-safe)
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        _sharedHelper = [[GameCenterPlugin alloc] init];
    });
    return _sharedHelper;
}

- (id)init {
    if ((self = [super init])) {
        gameCenterAvailable = [self isGameCenterAvailable];
        
        if (gameCenterAvailable) {
            NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
            [nc addObserver:self selector:@selector(authenticationChanged) name:GKPlayerAuthenticationDidChangeNotificationName object:nil];
        }
    }
    return self;
}

- (BOOL)isGameCenterAvailable {
    // check for presence of GKLocalPlayer API
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    
    // check if the device is running iOS 4.1 or later
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer
                                           options:NSNumericSearch] != NSOrderedAscending);
    
    return (gcClass && osVersionSupported);
}

#pragma mark Authentication

- (void)authenticationChanged {
    if ([GKLocalPlayer localPlayer].isAuthenticated && !userAuthenticated) {
        NSLog(@"Authentication changed: player authenticated.");
        userAuthenticated = TRUE;
        
        [self loadLeaderBoardInfo];
        [self loadAchievements];
        
    } else if (![GKLocalPlayer localPlayer].isAuthenticated && userAuthenticated) {
        NSLog(@"Authentication changed: player not authenticated.");
        userAuthenticated = FALSE;
    }
}

- (void)authenticateLocalUserOnViewController:(UIViewController*)viewController
                            setCallbackObject:(id)obj
                            withPauseSelector:(SEL)selector
{
    if (!gameCenterAvailable) {
        NSLog(@"{gamecenter} gameCenterUnavailable");
        return;
    }
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    NSLog(@"{gamecenter} Authenticating local user...");
    if (localPlayer.authenticated == NO) {
        [localPlayer setAuthenticateHandler:^(UIViewController* authViewController, NSError *error) {
            if (authViewController != nil) {
                if (obj) {
                    [obj performSelector:selector withObject:nil afterDelay:0];
                }
                
                [viewController presentViewController:authViewController animated:YES completion:^ {
                }];
            } else if (error != nil) {
                // process error
                NSLog(@"{gamecenter} error authenticated! %@",error);
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Game Center"
//                        message:@"Player is not signed in. Please sign in to Game Center first." delegate:self.teaLeafAppDelegate cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
            }
        }];
    }
    else {
        NSLog(@"{gamecenter} Already authenticated!");
    }
}

#pragma mark Leaderboards

- (void)loadLeaderBoardInfo
{
    [GKLeaderboard loadLeaderboardsWithCompletionHandler:^(NSArray *leaderboards, NSError *error) {
        self.leaderboards = leaderboards;
    }];
}

- (void)reportScore:(int64_t)score forLeaderboardID:(NSString*)identifier
{
    GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier: identifier];
    scoreReporter.value = score;
    scoreReporter.context = 0;
    
    [GKScore reportScores:@[scoreReporter] withCompletionHandler:^(NSError *error) {
        if (error == nil) {
            NSLog(@"Score reported successfully!");
        } else {
            NSLog(@"Unable to report score!");
        }
    }];
}

- (void)showLeaderboardOnViewController:(UIViewController*)viewController leaderBoardId:(NSString*)leaderBoardId
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil) {
        gameCenterController.gameCenterDelegate = self;
        gameCenterController.viewState = GKGameCenterViewControllerStateLeaderboards;
        gameCenterController.leaderboardIdentifier = leaderBoardId;
        
        [viewController presentViewController: gameCenterController animated: YES completion:nil];
    }
}

- (void)showAchievementOnViewController:(UIViewController*)viewController
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil) {
        gameCenterController.gameCenterDelegate = self;
        gameCenterController.viewState = GKGameCenterViewControllerStateAchievements;
        
        [viewController presentViewController: gameCenterController animated: YES completion:nil];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark Achievements

- (void)reportAchievementIdentifier: (NSString*) identifier percentComplete: (float) percent
{
    GKAchievement *achievement = [self getAchievementForIdentifier:identifier];
    if (achievement && achievement.percentComplete != 100.0) {
        achievement.percentComplete = percent;
        achievement.showsCompletionBanner = YES;
        
        [GKAchievement reportAchievements:@[achievement] withCompletionHandler:^(NSError *error) {
            if (error != nil) {
                NSLog(@"Error while reporting achievement: %@", error.description);
            }
        }];
    }
}

- (void)loadAchievements
{
    self.achievementsDictionary = [[NSMutableDictionary alloc] init];
    [GKAchievement loadAchievementsWithCompletionHandler:^(NSArray *achievements, NSError *error) {
        if (error != nil) {
            // Handle the error.
            NSLog(@"Error while loading achievements: %@", error.description);
        }
        else if (achievements != nil) {
            // Process the array of achievements.
            for (GKAchievement* achievement in achievements)
                self.achievementsDictionary[achievement.identifier] = achievement;
        }
    }];
}

- (GKAchievement*)getAchievementForIdentifier: (NSString*) identifier
{
    GKAchievement *achievement = [self.achievementsDictionary objectForKey:identifier];
    if (achievement == nil) {
        achievement = [[GKAchievement alloc] initWithIdentifier:identifier];
        self.achievementsDictionary[achievement.identifier] = achievement;
    }
    return achievement;
}

- (void)resetAchievements
{
    // Clear all locally saved achievement objects.
    self.achievementsDictionary = [[NSMutableDictionary alloc] init];
    // Clear all progress saved on Game Center.
    [GKAchievement resetAchievementsWithCompletionHandler:^(NSError *error)
     {
         if (error != nil) {
             // handle the error.
             NSLog(@"Error while reseting achievements: %@", error.description);
             
         }
     }];
}

- (void)completeMultipleAchievements:(NSArray*)achievements
{
    [GKAchievement reportAchievements:achievements withCompletionHandler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"Error while reporting achievements: %@", error.description);
        }
    }];
}

#pragma mark Challenges

- (void)registerListener:(id<GKLocalPlayerListener>)listener
{
    [[GKLocalPlayer localPlayer] registerListener:listener];
}

// called when you complete a challenge sent by a friend
- (void)player:(GKPlayer *)player didCompleteChallenge:(GKChallenge *)challenge issuedByFriend:(GKPlayer *)friendPlayer
{
    UIAlertView *completedChallenge = [[UIAlertView alloc] initWithTitle:@"Challenge completed" message:@"Congratulations!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [completedChallenge show];
}

// called when a friend completed a challenge issued by you
- (void)player:(GKPlayer *)player issuedChallengeWasCompleted:(GKChallenge *)challenge byFriend:(GKPlayer *)friendPlayer
{
    NSMutableString *fr = [[NSMutableString alloc] initWithString:@"Your friend "];
    [fr appendString:[friendPlayer displayName]];
    [fr appendString:@" has successfully completed the challenge!"];
    UIAlertView *completedChallenge = [[UIAlertView alloc] initWithTitle:@"Challenge completed" message:fr delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [completedChallenge show];
}

// called when you click on the challenge notification, while not playing the game
- (void)player:(GKPlayer *)player wantsToPlayChallenge:(GKChallenge *)challenge
{
    [self performSegueWithIdentifier:@"startPlaying" sender:self];
}

// called when you received a challenge while playing the game
- (void)player:(GKPlayer *)player didReceiveChallenge:(GKChallenge *)challenge
{
    NSMutableString *fr = [[NSMutableString alloc] initWithString:@"Your friend "];
    [fr appendString:[player displayName]];
    [fr appendString:@" has invited you to complete a challenge:\n"];
    [fr appendString:[challenge message]];
    UIAlertView *theChallenge = [[UIAlertView alloc] initWithTitle:@"Want to take the challenge?" message:fr delegate:self cancelButtonTitle:@"Challenge accepted" otherButtonTitles:@"No", nil];
    [theChallenge show];
}
//JS Method

- (void) showLeaderBoard:(NSDictionary *)jsonObject {
	@try {
        NSString *leaderBoardId = [jsonObject valueForKey:@"leaderBoardId"];
        if ([leaderBoardId isEqualToString:@""]) {
            leaderBoardId = nil;
        }
        [self showLeaderboardOnViewController:self.tealeafViewController leaderBoardId:leaderBoardId];
	}
	@catch (NSException *exception) {
        NSLog(@"{gamecenter} Failure during showLeaderboard: %@", exception);
	}
}

- (void) showAchievement:(NSDictionary *)jsonObject {
	@try {
        [self showAchievementOnViewController:self.tealeafViewController];
	}
	@catch (NSException *exception) {
        NSLog(@"{gamecenter} Failure during showLeaderboard: %@", exception);
	}
}

- (void) postScore:(NSDictionary *)jsonObject {
	@try {
        NSString *leaderBoardId = [jsonObject valueForKey:@"leaderBoardId"];
        int64_t score = [[jsonObject valueForKey:@"score"] integerValue];
        NSLog(@"{gamecenter} postScore: %@ %lld", leaderBoardId, score);
        [[GameCenterPlugin defaultHelper] reportScore:score forLeaderboardID:leaderBoardId];
	}
	@catch (NSException *exception) {
        NSLog(@"{gamecenter} Failure during postScore: %@", exception);
	}
}

- (void) postAchievement:(NSDictionary *)jsonObject {
	@try {
        NSString *achievementId = [jsonObject valueForKey:@"achievementId"];
        float percentComplete = [[jsonObject valueForKey:@"percentComplete"] floatValue];
        NSLog(@"{gamecenter} postAchievement: %@ %f", achievementId, percentComplete);
        [[GameCenterPlugin defaultHelper] reportAchievementIdentifier:achievementId percentComplete:percentComplete];
	}
	@catch (NSException *exception) {
        NSLog(@"{gamecenter} Failure during postAchievement: %@", exception);
	}
}

@end

