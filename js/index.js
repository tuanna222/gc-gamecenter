var hasNativeEvents = NATIVE && NATIVE.plugins && NATIVE.plugins.sendRequest;

var gamecenter = Class(function () {
	this.init = function () {
	}

	this.showLeaderBoard = function (leaderBoardId) {
		logger.log("{gamecenter} JS Showing leaderboard");
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			logger.log("sendEvent GameCenterPlugin...");
			NATIVE.plugins.sendEvent("GameCenterPlugin", "showLeaderBoard",
				JSON.stringify({leaderBoardId: leaderBoardId}));
		}
	};

	this.showAchievement = function () {
		logger.log("{gamecenter} JS Showing achievement");
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			logger.log("sendEvent GameCenterPlugin...");
			NATIVE.plugins.sendEvent("GameCenterPlugin", "showAchievement",
				JSON.stringify({}));
		}
	};

	this.postScore = function (score, leaderBoardId) {
		logger.log("{gamecenter} JS postScore");
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			logger.log("sendEvent GameCenterPlugin...");
			NATIVE.plugins.sendEvent("GameCenterPlugin", "postScore",
				JSON.stringify({score: score, leaderBoardId: leaderBoardId}));
		}
	};

	this.postAchievement = function (percentComplete, achievementId) {
		logger.log("{gamecenter} JS postAchievement");
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			logger.log("sendEvent GameCenterPlugin...");
			NATIVE.plugins.sendEvent("GameCenterPlugin", "postAchievement",
				JSON.stringify({percentComplete: percentComplete, achievementId: achievementId}));
		}
	};

});

exports = new gamecenter();
